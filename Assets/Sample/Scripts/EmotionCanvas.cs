﻿/**
*
* Copyright (c) 2021 XZIMG Limited , All Rights Reserved
* No part of this software and related documentation may be used, copied,
* modified, distributed and transmitted, in any form or by any means,
* without the prior written permission of XZIMG Limited
*
* contact@xzimg.com, www.xzimg.com
*
*/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using XZIMG;

[RequireComponent(typeof(Canvas))]
public class EmotionCanvas : MonoBehaviour
{
    #region Public properties
    public XmgMagicFace face;
    #endregion

    #region Private properties
    private Slider m_NeutralSlider;
    private Slider m_HappySlider;
    private Slider m_AstronishedSlider;
    private Slider m_SadSlider;
    private Slider m_AngrySlider;
    #endregion

    private void Awake()
    {
        var emotionManager = FindObjectOfType<XmgMagicFaceEmotionManager>();
        if (emotionManager == null || !emotionManager.isActiveAndEnabled)
            Debug.LogWarning("No active XmgMagicFaceEmotionManager detected. Add or enable it to display emotions.");

        // Set Camera
        var canvas = GetComponent<Canvas>();
        canvas.worldCamera = FindObjectOfType<XmgCameraManager>().GetComponent<Camera>();

        // Get sliders
        var sliders = GetComponentsInChildren<Slider>();

        foreach (var slider in sliders)
        {
            switch (slider.name)
            {
                case "Neutral-Slider":
                    m_NeutralSlider = slider;
                    break;
                case "Happy-Slider":
                    m_HappySlider = slider;
                    break;
                case "Astronished-Slider":
                    m_AstronishedSlider = slider;
                    break;
                case "Sad-Slider":
                    m_SadSlider = slider;
                    break;
                case "Angry-Slider":
                    m_AngrySlider = slider;
                    break;
            }
        }
    }

    private void OnEnable()
    {
        face.FaceDataUpdated += OnFaceDataUpdated;
    }

    private void OnDisable()
    {
        face.FaceDataUpdated -= OnFaceDataUpdated;
    }

    private void OnFaceDataUpdated(XmgMagicFaceData faceData)
    {
        m_NeutralSlider.value = faceData.m_faceData.m_emotions.getEmotion(0);
        m_HappySlider.value = faceData.m_faceData.m_emotions.getEmotion(1);
        m_AstronishedSlider.value = faceData.m_faceData.m_emotions.getEmotion(2);
        m_SadSlider.value = faceData.m_faceData.m_emotions.getEmotion(3);
        m_AngrySlider.value = faceData.m_faceData.m_emotions.getEmotion(4);
    }
}
