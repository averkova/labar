﻿//================================================================================================================================
//
//  Copyright (c) 2015-2021 VisionStar Information Technology (Shanghai) Co., Ltd. All Rights Reserved.
//  EasyAR is the registered trademark or trademark of VisionStar Information Technology (Shanghai) Co., Ltd in China
//  and other countries for the augmented reality technology developed by VisionStar Information Technology (Shanghai) Co., Ltd.
//
//================================================================================================================================

using Common;
using easyar;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace SurfaceTracking
{
    public class UIControll : MonoBehaviour
    {
        public ARSession Session;
        public TouchControll TouchControl;

        private SurfaceTrackerFrameFilter tracker;

        private void Awake()
        {
            tracker = Session.GetComponentInChildren<SurfaceTrackerFrameFilter>();
            Session.StateChanged += (state) =>
            {
                if (state == ARSession.SessionState.Ready)
                {
                    TouchControl.TurnOn(TouchControl.transform, Session.Assembly.Camera, false, false, true, true);
                }
            };
        }

        private void Update()
        {
            if (Input.touchCount == 1 && !EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
            {
                var touch = Input.touches[0];
                if (touch.phase == TouchPhase.Moved)
                {
                    var viewPoint = new Vector2(touch.position.x / Screen.width, touch.position.y / Screen.height);
                    var coord = Session.ImageCoordinatesFromScreenCoordinates(viewPoint);
                    if (tracker && tracker.Tracker != null && coord.OnSome)
                    {
                        tracker.Tracker.alignTargetToCameraImagePoint(coord.Value.ToEasyARVector());
                    }
                }
            }
        }
    }
}
