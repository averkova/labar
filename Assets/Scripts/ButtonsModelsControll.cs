using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonsModelsControll : MonoBehaviour
{
    [SerializeField]
    GameObject objectToSpawn;

    public void SetObject()
    {
        List<RaycastHit> hits = new List<RaycastHit>();

        //if (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
        //{
        var surfaceTarget = GameObject.Find("SurfaceTarget");
        surfaceTarget.transform.parent = objectToSpawn.transform;
        Instantiate(objectToSpawn, new Vector3(Screen.width / 2, Screen.height / 2, 0), objectToSpawn.transform.rotation);   
        //}
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
